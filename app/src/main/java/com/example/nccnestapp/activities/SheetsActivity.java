/*
 * Copyright (C) 2019 The CSC 240 Instructors & Students.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.example.nccnestapp.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.example.nccnestapp.R;
import com.example.nccnestapp.utilities.PantryGuest;
import com.example.nccnestapp.utilities.SheetsTaskListener;

import java.util.ArrayList;
import java.util.List;

import static com.example.nccnestapp.utilities.Constants.SHEET_ID;
import static com.example.nccnestapp.utilities.Constants.SHEET_RANGE;

public class SheetsActivity extends AbstractActivity {

    static List<PantryGuest> mResults;


    /**
     *
     *
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sheets);
        mResults = new ArrayList<>();
        makeSheetsApiCall();
    }


    @Override
    protected void makeSheetsApiCall() {
        //  For Guest Sheet:
        getResultsFromApi(new SheetsTaskListener() {

            @Override
            public String getSheetId() {
                return SHEET_ID;
            }

            @Override
            public String getSheetRange() {
                return SHEET_RANGE;
            }


            @Override
            public void onTaskCompleted() {
                ((TextView)findViewById(R.id.admin_list)).setText(TextUtils.join("\n", mResults));
            }


            @Override
            public void processData(List<List<Object>> values) {
                if (values != null) {
                    values.remove(0);

                    for (List row : values) {
                        PantryGuest guest = new PantryGuest()
                                .setEmail(row.get(1)).setPin(row.get(2))
                                .setLastName(row.get(3)).setFirstName(row.get(4))
                                .setPhone(row.get(5)).setStreet(row.get(6))
                                .setCity(row.get(7)).setState(row.get(8))
                                .setZip(row.get(9)).setSchoolID(row.get(10))
                                .setGender(row.get(11)).setAge(row.get(12))
                                .setSize(row.get(13)).setIncome(row.get(14))
                                .setFoodStamps(row.get(15)).setFoodPrograms(row.get(16))
                                .setStatusEmploy(row.get(17)).setStatusHealth(row.get(18))
                                .setStatusHousing(row.get(19)).setStatusChild(row.get(20))
                                .setChildUnder1(row.get(21)).setChild1to5(row.get(22))
                                .setChild6to12(row.get(23)).setChild13to18(row.get(24))
                                .setDietNeeds(row.get(25)).setFoundFrom(row.get(26) + " " + row.get(27))
                                .setComments(row.get(28)).setHelpedBy(row.get(29));
                        mResults.add(guest);
                    }
                }
            }
        });
    }
}