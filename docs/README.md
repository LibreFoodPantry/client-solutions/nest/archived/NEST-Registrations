# NCC-NEST-Registrations
App research for the Nassau Community College food pantry

***Use these NEST forms with ODK Collect (copy to root\odk\forms):*** 
https://www.dropbox.com/sh/v0gkvd1sng7sz11/AABoBXH0jrUWgdYdD5RTRGMMa?dl=0   (In progress)


---
Copyright &copy; 2019 The LibreFoodPantry Community. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
